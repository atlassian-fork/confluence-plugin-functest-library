package it.com.atlassian.confluence.plugin.functest.selenium;

import com.atlassian.confluence.plugin.functest.ConfluencePluginSeleniumTestCaseBase;
import com.atlassian.confluence.plugin.functest.helper.UPMHelper;
import com.atlassian.confluence.plugin.functest.util.ClasspathResourceUtil;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.HelperFactory;
import com.atlassian.confluence.plugin.functest.util.PluginMetadataUtil;
import junit.framework.Assert;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class SimpleSeleniumConfluencePluginSeleniumTestCase extends ConfluencePluginSeleniumTestCaseBase
{

    public void testDashboardAccessible()
    {
        tester.open(tester.getContextPath() + "/dashboard.action");
        tester.waitForPageToLoad();

        assertTrue(tester.isTextPresent("Dashboard"));
    }

    public void testFlushCache()
    {
        tester.flushCaches();
    }

    public void testSoapApiAccessible()
    {
        PageHelper pageHelper = HelperFactory.createPageHelper(tester);

        String spaceKey = "ds";
        String pageTitle = "testSoapApiAccessible";
        
        pageHelper.setSpaceKey(spaceKey);
        pageHelper.setTitle(pageTitle);
        pageHelper.setContent("Cheese");

        assertTrue(pageHelper.create());

        tester.open(tester.getContextPath() + "/display/" + spaceKey + "/" + pageTitle);
        assertTrue(tester.isTextPresent("Cheese"));
    }

    public void testXmlRpcApiAccessible()
    {
        String spaceKey = "ds";
        String pageTitle = "testSoapApiAccessible";

        testSoapApiAccessible();

        PageHelper pageHelper = HelperFactory.createPageHelper(tester);

        pageHelper.setSpaceKey(spaceKey);
        pageHelper.setTitle(pageTitle);

        // Search is done via xmlrpc
        assertTrue(pageHelper.findBySpaceKeyAndTitle() > 0);
    }

    public void testRestoreBackup() throws IOException
    {
        File siteExportContainingSeleniumSpace = ClasspathResourceUtil.getClassPathResourceAsTempFile(
                "site-export-containing-selenium-space.zip",
                getClass().getClassLoader(),
                ".zip"
        );

        tester.restoreData(siteExportContainingSeleniumSpace);
        tester.open(tester.getContextPath() + "/display/SS");

        assertTrue(tester.isTextPresent("Selenium Space"));
    }

    public void testInstallPlugin() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException
    {
        File pageHistorySliderPlugin = ClasspathResourceUtil.getClassPathResourceAsTempFile("page-history-slider-1.0.jar",
                getClass().getClassLoader(), ".jar");

        tester.installPlugin(pageHistorySliderPlugin);
        try
        {
            String pluginKey = PluginMetadataUtil.getPluginKey(PluginMetadataUtil.getPluginDescriptorDom(pageHistorySliderPlugin));
            pluginKey = UPMHelper.buildUPMPluginOrModuleKey(pluginKey);
            assertThat(tester.isPluginInstalled(pluginKey), is(true));
            tester.uninstallPlugin(pluginKey);
        }
        finally
        {
            tester.dropEscalatedPrivileges();
        }
    }
}
