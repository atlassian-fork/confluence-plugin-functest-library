package com.atlassian.confluence.plugin.functest.module.xmlrpc.content;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

public class ContentHelperServiceImpl implements ContentHelperService
{
    private ContentHelperService contentHelperServiceDelegate;

    private PlatformTransactionManager transactionManager;

    public void setContentHelperServiceDelegate(ContentHelperService contentHelperServiceDelegate)
    {
        this.contentHelperServiceDelegate = contentHelperServiceDelegate;
    }

    public void setTransactionManager(PlatformTransactionManager transactionManager)
    {
        this.transactionManager = transactionManager;
    }

    public boolean addWatch(final String authenticationToken, final String entityId)
    {
        return (Boolean) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        return contentHelperServiceDelegate.addWatch(authenticationToken, entityId);
                    }
                }
        );
    }

    public boolean removeWatch(final String authenticationToken, final String entityId)
    {
        return (Boolean) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        return contentHelperServiceDelegate.removeWatch(authenticationToken, entityId);
                    }
                }
        );
    }

    public boolean isWatched(final String authenticationToken, final String entityId)
    {
        return (Boolean) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        return contentHelperServiceDelegate.isWatched(authenticationToken, entityId);
                    }
                }
        );
    }

    public boolean markFavorite(final String authenticationToken, final String entityId)
    {
        return (Boolean) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        return contentHelperServiceDelegate.markFavorite(authenticationToken, entityId);
                    }
                }
        );
    }

    public boolean unmarkFavorite(final String authentictionToken, final String entityId)
    {
        return (Boolean) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        return contentHelperServiceDelegate.unmarkFavorite(authentictionToken, entityId);
                    }
                }
        );
    }

    public boolean isFavorite(final String authenticationToken, final String entityId)
    {
        return (Boolean) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        return contentHelperServiceDelegate.isFavorite(authenticationToken, entityId);
                    }
                }
        );
    }

    public String login(String s, String s1)
    {
        return null;
    }

    public boolean logout(String s)
    {
        return false;
    }
}
