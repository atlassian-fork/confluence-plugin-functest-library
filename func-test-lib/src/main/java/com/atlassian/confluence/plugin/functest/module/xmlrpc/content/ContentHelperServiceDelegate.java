package com.atlassian.confluence.plugin.functest.module.xmlrpc.content;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Labelable;
import com.atlassian.confluence.labels.Namespace;
import com.atlassian.confluence.mail.notification.Notification;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.userstatus.FavouriteManager;
import com.atlassian.user.User;

public class ContentHelperServiceDelegate implements ContentHelperService
{
    private LabelManager labelManager;

    private NotificationManager notificationManager;

    private PageManager pageManager;

    private FavouriteManager favouriteManager;

    public void setLabelManager(LabelManager labelManager)
    {
        this.labelManager = labelManager;
    }

    public void setNotificationManager(NotificationManager notificationManager)
    {
        this.notificationManager = notificationManager;
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public void setFavouriteManager(FavouriteManager favouriteManager) {
        this.favouriteManager = favouriteManager;
    }

    private AbstractPage getPage(String pageIdStr)
    {
        long pageId = Long.parseLong(pageIdStr);
        return pageManager.getAbstractPage(pageId);
    }

    private Notification getNotification(AbstractPage abstractPage)
    {
        return null == abstractPage
                ? null
                : notificationManager.getNotificationByUserAndContent(AuthenticatedUserThreadLocal.get(), abstractPage);
    }

    public boolean addWatch(String authenticationToken, String entityId)
    {
        AbstractPage abstractPage = getPage(entityId);

        if (null == abstractPage)
            return false;

        if (null != getNotification(abstractPage))
            return false;

        notificationManager.addContentNotification(
                AuthenticatedUserThreadLocal.get(),
                abstractPage
        );

        return true;
    }

    public boolean removeWatch(String authenticationToken, String entityId)
    {
        AbstractPage abstractPage = getPage(entityId);
        Notification notification;

        if (null == abstractPage)
            return false;

        if (null == (notification = getNotification(abstractPage)))
            return false;

        notificationManager.removeNotification(notification);

        return true;
    }

    public boolean isWatched(String authenticationToken, String entityId)
    {
        return null != getNotification(
                getPage(entityId)
        );
    }

    public boolean markFavorite(String authenticationToken, String entityId)
    {
        AbstractPage contentEntityObject = getPage(entityId);

        if (null == contentEntityObject) {
            return false;
        }

        ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();

        if (favouriteManager.isUserFavourite(currentUser, contentEntityObject))
            return false;

        favouriteManager.addPageToFavourites(currentUser, contentEntityObject);
        return true;
    }

    public boolean unmarkFavorite(String authentictionToken, String entityId)
    {
        AbstractPage contentEntityObject = getPage(entityId);

        if (null == contentEntityObject)
            return false;

        ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();

        if (!favouriteManager.isUserFavourite(currentUser, contentEntityObject))
            return false;

        labelManager.removeLabel(contentEntityObject, new Label(LabelManager.FAVOURITE_LABEL, Namespace.PERSONAL, currentUser));
        labelManager.removeLabel(contentEntityObject, new Label(LabelManager.FAVOURITE_LABEL_YANKEE, Namespace.PERSONAL, currentUser));

        return true;
    }

    public boolean isFavorite(String authenticationToken, String entityId)
    {
        AbstractPage ceo = getPage(entityId);
        return null != ceo && favouriteManager.isUserFavourite(AuthenticatedUserThreadLocal.get(), ceo);
    }

    public String login(String s, String s1)
    {
        return null;
    }

    public boolean logout(String s)
    {
        return false;
    }
}
