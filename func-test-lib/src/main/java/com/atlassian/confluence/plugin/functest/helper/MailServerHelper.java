package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class MailServerHelper extends AbstractHelper
{

    private static final Logger LOG = Logger.getLogger(MailServerHelper.class);

    private long id;

    private String name;

    private String fromAddress;

    private String prefix;

    private String address;

    private String userName;

    private String password;

    private String jndiLocation;

    public MailServerHelper(final ConfluenceWebTester confluenceWebTester)
    {
        this(confluenceWebTester, 0);
    }

    public MailServerHelper(final ConfluenceWebTester confluenceWebTester, final long id)
    {
        super(confluenceWebTester);
        setId(id);
        setPrefix("[confluence]");
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getFromAddress()
    {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress)
    {
        this.fromAddress = fromAddress;
    }

    public String getPrefix()
    {
        return prefix;
    }

    public void setPrefix(String prefix)
    {
        this.prefix = prefix;
    }

    public String getAddress()
    {
        return null == address
                ? null
                : (address.indexOf(":") < 0 ? address + ":25" : address);
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getJndiLocation()
    {
        return jndiLocation;
    }

    public void setJndiLocation(String jndiLocation)
    {
        this.jndiLocation = jndiLocation;
    }

    private void fromMap(final Map mailServerStructure)
    {
        setId(Long.parseLong((String) mailServerStructure.get("id")));
        setName((String) mailServerStructure.get("name"));
        setFromAddress((String) mailServerStructure.get("fromAddress"));
        setPrefix((String) mailServerStructure.get("prefix"));
        setJndiLocation((String) mailServerStructure.get("jndiLocation"));
        setUserName((String) mailServerStructure.get("userName"));
        setPassword((String) mailServerStructure.get("password"));
        setAddress((String) mailServerStructure.get("address"));
    }

    private Map toMap()
    {
        final Hashtable<String, String> hashtable = new Hashtable<String, String>();

        hashtable.put("id", String.valueOf(getId()));
        if (null != getName())
        {
            hashtable.put("name", getName());
        }
        if (null != getFromAddress())
        {
            hashtable.put("fromAddress", getFromAddress());
        }
        if (null != getPrefix())
        {
            hashtable.put("prefix", getPrefix());
        }
        if (null != getJndiLocation())
        {
            hashtable.put("jndiLocation", getJndiLocation());
        }
        if (null != getUserName())
        {
            hashtable.put("userName", getUserName());
        }
        if (null != getPassword())
        {
            hashtable.put("password", getPassword());
        }
        if (null != getAddress())
        {
            hashtable.put("address", getAddress());
        }

        return hashtable;
    }

    @SuppressWarnings("unchecked")
    public Map<String, String> getMailServerIdMap()
    {
        String authenticationToken = null;

        try
        {
            final XmlRpcClient xmlRpcClient = confluenceWebTester.getXmlRpcClient();

            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            return (Map<String, String>) xmlRpcClient.execute("functest-mailserver.getMailServerIdsAndNames",
                    new Vector<String>(
                            Arrays.asList(
                                    authenticationToken
                            )
                    ));

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final XmlRpcException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return Collections.EMPTY_MAP;
    }

    public List getMailServerIdsByName()
    {
        final List<Long> mailServerIds = new ArrayList<Long>();
        final Map<String, String> mailServerIdMap = getMailServerIdMap();

        for (Map.Entry<String, String> entry : mailServerIdMap.entrySet())
        {
            if (StringUtils.equals(entry.getValue(), getName()))
            {
                mailServerIds.add(Long.valueOf(entry.getKey()));
            }
        }

        return mailServerIds;
    }

    public boolean create()
    {
        String authenticationToken = null;

        try
        {
            final XmlRpcClient xmlRpcClient = confluenceWebTester.getXmlRpcClient();
            final long mailServerId;

            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            mailServerId = Long.parseLong((String) xmlRpcClient.execute("functest-mailserver.createMailServer",
                    new Vector<Object>(
                            Arrays.asList(
                                    authenticationToken,
                                    toMap()
                            )
                    )));

            setId(mailServerId);
            return true;

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final XmlRpcException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return false;
    }

    public boolean update()
    {
        if (0 < getId())
        {
            String authenticationToken = null;

            try
            {
                final XmlRpcClient xmlRpcClient = confluenceWebTester.getXmlRpcClient();

                authenticationToken = confluenceWebTester.loginToXmlRPcService();
                return (Boolean) xmlRpcClient.execute("functest-mailserver.updateMailServer",
                        new Vector<Object>(
                                Arrays.asList(
                                        authenticationToken,
                                        toMap()
                                )
                        ));

            }
            catch (final MalformedURLException mUrlE)
            {
                LOG.error("Invalid RPC URL specified.", mUrlE);
            }
            catch (final XmlRpcException se)
            {
                LOG.error("Service request denied.", se);
            }
            catch (final RemoteException re)
            {
                LOG.error("There's an error in Confluence.", re);
            }
            catch (final IOException ioe)
            {
                LOG.error("Can't talk to Confluence.", ioe);
            }
            finally
            {
                confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
            }
        }

        return false;
    }

    public boolean read()
    {
        if (0 < getId())
        {
            String authenticationToken = null;

            try
            {
                final XmlRpcClient xmlRpcClient = confluenceWebTester.getXmlRpcClient();
                final Map mailServerStruct;

                authenticationToken = confluenceWebTester.loginToXmlRPcService();
                mailServerStruct = (Map) xmlRpcClient.execute("functest-mailserver.readMailServer",
                        new Vector<Object>(
                                Arrays.asList(
                                        authenticationToken,
                                        String.valueOf(getId())
                                )
                        ));

                fromMap(mailServerStruct);

                return true;

            }
            catch (final MalformedURLException mUrlE)
            {
                LOG.error("Invalid RPC URL specified.", mUrlE);
            }
            catch (final XmlRpcException se)
            {
                LOG.error("Service request denied.", se);
            }
            catch (final RemoteException re)
            {
                LOG.error("There's an error in Confluence.", re);
            }
            catch (final IOException ioe)
            {
                LOG.error("Can't talk to Confluence.", ioe);
            }
            finally
            {
                confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
            }
        }

        return false;
    }

    public boolean delete()
    {
        if (0 < getId())
        {
            String authenticationToken = null;

            try
            {
                final XmlRpcClient xmlRpcClient = confluenceWebTester.getXmlRpcClient();

                authenticationToken = confluenceWebTester.loginToXmlRPcService();
                return (Boolean) xmlRpcClient.execute("functest-mailserver.deleteMailServer",
                        new Vector<Object>(
                                Arrays.asList(
                                        authenticationToken,
                                        String.valueOf(getId())
                                )
                        ));

            }
            catch (final MalformedURLException mUrlE)
            {
                LOG.error("Invalid RPC URL specified.", mUrlE);
            }
            catch (final XmlRpcException se)
            {
                LOG.error("Service request denied.", se);
            }
            catch (final RemoteException re)
            {
                LOG.error("There's an error in Confluence.", re);
            }
            catch (final IOException ioe)
            {
                LOG.error("Can't talk to Confluence.", ioe);
            }
            finally
            {
                confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
            }
        }

        return false;
    }
}
