package com.atlassian.confluence.plugin.functest.module.xmlrpc.rendering;

import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.rpc.SecureRpc;

import javax.xml.stream.XMLStreamException;


public interface RenderingHelperService extends SecureRpc
{
    String convertMarkupToXhtml(String authToken, String markup, String pageId);

    String convertXhtmlToMarkup(String authToken, String xhtml, String pageId);

    String getViewFormat(String authToken, String pageId)  throws XMLStreamException, XhtmlException;
}
