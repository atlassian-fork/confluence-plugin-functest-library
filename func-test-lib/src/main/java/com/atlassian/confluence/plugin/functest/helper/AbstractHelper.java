package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;

public abstract class AbstractHelper implements Helper
{
    protected final ConfluenceWebTester confluenceWebTester;

    protected AbstractHelper(final ConfluenceWebTester confluenceWebTester)
    {
        this.confluenceWebTester = confluenceWebTester;
    }

    public ConfluenceWebTester getConfluenceWebTester()
    {
        return confluenceWebTester;
    }
}
