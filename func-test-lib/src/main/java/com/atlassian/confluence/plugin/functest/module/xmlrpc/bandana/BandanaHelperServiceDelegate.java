package com.atlassian.confluence.plugin.functest.module.xmlrpc.bandana;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;
import org.apache.commons.lang.StringUtils;

import java.util.Hashtable;
import java.util.Map;


public class BandanaHelperServiceDelegate implements BandanaHelperService
{

    private BandanaManager bandanaManager;

    private PermissionManager permissionManager;

    public BandanaManager getBandanaManager()
    {
        return bandanaManager;
    }

    public void setBandanaManager(BandanaManager bandanaManager)
    {
        this.bandanaManager = bandanaManager;
    }

    public PermissionManager getPermissionManager()
    {
        return permissionManager;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    private boolean isCurrentUserAnAdmin()
    {
        final User user = AuthenticatedUserThreadLocal.get();

        return null != user && getPermissionManager().hasPermission(
                user,
                Permission.ADMINISTER,
                PermissionManager.TARGET_APPLICATION);
    }

    private void checkBandanaKey(String key) throws RemoteException
    {
        if (StringUtils.isBlank(key))
        {
            throw new RemoteException("Key must not be blank");
        }
    }

    private ConfluenceBandanaContext getBandanaContext(String spaceKey)
    {
        return StringUtils.isBlank(spaceKey) ? new ConfluenceBandanaContext() : new ConfluenceBandanaContext(spaceKey);
    }

    public Boolean setValue(String authenticationToken, String spaceKey, String key, String value) throws RemoteException
    {
        if (!isCurrentUserAnAdmin())
        {
            throw new RemoteException("You do not have admin permission.");
        }

        checkBandanaKey(key);
        getBandanaManager().setValue(getBandanaContext(spaceKey), key, value);

        return Boolean.TRUE;
    }

    public Boolean removeValue(String authenticationToken, String spaceKey, String key) throws RemoteException
    {
        if (!isCurrentUserAnAdmin())
        {
            throw new RemoteException("You do not have admin permission.");
        }

        checkBandanaKey(key);
        getBandanaManager().setValue(getBandanaContext(spaceKey), key, null);
        return Boolean.TRUE;
    }

    public Map<String, Object> getValue(String authenticationToken, String spaceKey, String key) throws RemoteException
    {
        final Object bandanaValue;
        final Map<String, Object> bandanaEntry;

        if (!isCurrentUserAnAdmin())
        {
            throw new RemoteException("You do not have admin permission.");
        }

        checkBandanaKey(key);

        bandanaEntry = new Hashtable<String, Object>();
        bandanaValue = getBandanaManager().getValue(getBandanaContext(spaceKey), key);

        if (null != bandanaValue)
        {
            if (StringUtils.isNotBlank(spaceKey))
            {
                bandanaEntry.put("spaceKey", spaceKey);
            }

            bandanaEntry.put("key", key);
            bandanaEntry.put("value", bandanaValue);
        }

        return bandanaEntry;
    }

    public String login(String s, String s1)
    {
        return null;
    }

    public boolean logout(String s)
    {
        return false;
    }
}
