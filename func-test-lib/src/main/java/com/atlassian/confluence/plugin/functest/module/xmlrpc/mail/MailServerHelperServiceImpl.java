package com.atlassian.confluence.plugin.functest.module.xmlrpc.mail;

import com.atlassian.confluence.rpc.RemoteException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Hashtable;

public class MailServerHelperServiceImpl implements MailServerHelperService
{
    private MailServerHelperService mailServerHelperServiceDelegate;

    private PlatformTransactionManager transactionManager;

    public void setMailServerHelperServiceDelegate(MailServerHelperService mailServerHelperServiceDelegate)
    {
        this.mailServerHelperServiceDelegate = mailServerHelperServiceDelegate;
    }

    public void setTransactionManager(PlatformTransactionManager transactionManager)
    {
        this.transactionManager = transactionManager;
    }

    public String createMailServer(final String authToken, final Hashtable mailServerStructure)
    {
        return (String) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        try
                        {
                            return mailServerHelperServiceDelegate.createMailServer(authToken, mailServerStructure);
                        }
                        catch (RemoteException re)
                        {
                            throw new RuntimeException(re);
                        }
                    }
                }
        );
    }

    public boolean updateMailServer(final String authToken, final Hashtable mailServerStructure)
    {
        return (Boolean) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        try
                        {
                            return mailServerHelperServiceDelegate.updateMailServer(authToken, mailServerStructure);
                        }
                        catch (RemoteException re)
                        {
                            throw new RuntimeException(re);
                        }
                    }
                }
        );
    }

    @SuppressWarnings("unchecked")
    public Hashtable<String, String>  readMailServer(final String authToken, final String mailServerId) throws RemoteException
    {
        return (Hashtable<String, String> ) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        try
                        {
                            return mailServerHelperServiceDelegate.readMailServer(authToken, mailServerId);
                        }
                        catch (RemoteException re)
                        {
                            throw new RuntimeException(re);
                        }
                    }
                }
        );
    }

    public boolean deleteMailServer(final String authToken, final String mailServerId)
    {
        return (Boolean) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        try
                        {
                            return mailServerHelperServiceDelegate.deleteMailServer(authToken, mailServerId);
                        }
                        catch (RemoteException re)
                        {
                            throw new RuntimeException(re);
                        }
                    }
                }
        );
    }


    @SuppressWarnings("unchecked")
    public Hashtable<String, String> getMailServerIdsAndNames(final String authToken)
    {
        return (Hashtable) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        try
                        {
                            return mailServerHelperServiceDelegate.getMailServerIdsAndNames(authToken);
                        }
                        catch (RemoteException re)
                        {
                            throw new RuntimeException(re);
                        }
                    }
                }
        );
    }

    public String login(final String s, final String s1)
    {
        return null;
    }

    public boolean logout(final String s)
    {
        return false;
    }
}
